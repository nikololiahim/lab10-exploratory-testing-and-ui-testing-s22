//> using scala "2.13.8"
//> using lib "org.scalatest::scalatest:3.2.12"
//> using lib "org.scalatestplus::scalatestplus-selenium:1.0.0-M2"
//>

import org.scalatest._
import org.scalatest.matchers._
import org.scalatestplus.selenium._
import org.openqa.selenium.WebDriver
import org.openqa.selenium.htmlunit.HtmlUnitDriver

class BlogSpec
    extends flatspec.AnyFlatSpec
    with should.Matchers
    with WebBrowser {

  implicit val webDriver: WebDriver = new HtmlUnitDriver

  val mainPage = "https://myanimelist.net/"

  "Highest ranking anime" should "be Fullmetal Alchemist" in {
    go.to(mainPage)
    val topAnime =
      xpath("/html/body/div[1]/div[2]/div[2]/div[2]/ul/li[1]/ul/li[2]/a")
    click on topAnime

    val top1Title = xpath(
      "/html/body/div[1]/div[2]/div[3]/div[2]/div[4]/table/tbody/tr[2]/td[2]/div/div[2]/h3/a"
    )

    assert(find(top1Title).get.text.contains("Fullmetal Alchemist"))
  }
}
